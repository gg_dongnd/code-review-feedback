#Hello world



### Các nhận xét khác


#### Linting phần test

Nên bỏ thư mục /test/ ra khỏi .eslintignore vì code trong phần test cũng phải được viết một cách chuyên nghiệp. Khách hàng review code của chúng ta chắc chắn sẽ không bỏ qua phần test.

Dự án hiện có hơn 2000 errors về convention trong thư mục test.


#### Quản lý dependencies

- istanbul, nsp xuất hiện cả trong dependencies và devDependencies, khác version.

- eslint, eslint-config-ggc đang bị outdated. Cần chú ý cập nhật dependencies lên các phiên bản mới nhất ngay khi có thể.

Một trong các best practices khi khai báo dependencies trong package.json là nên dùng X-Ranges:

Don't:


```
// Caret Ranges
  "dependencies": {
    "cors": "^2.5.2",
    "helmet": "^1.3.0",
    "istanbul": "^0.4.5",
    "loopback": "^2.22.0",
  },
```

Do:

```
// X-Ranges
  "dependencies": {
    "cors": "2.x.x",
    "helmet": "1.x.x",
    "istanbul": "0.x.x",
    "loopback": "2.x.x",
  },
```


Semantic versioning định nghĩa tên phiên bản theo dạng MAJOR.MINOR.PATCH, trong đó, MAJOR chỉ thay đổi khi APIs không tương thích.

2.x.x so khớp từ 2.0.0 đến 2.9.9, khá linh hoạt để không bị outdated, đồng thời trong khoảng này, APIs không thay đổi nên vẫn đảm bảo an toàn cho ứng dụng.

Trong khi đó, ^x.x.x có thể cho ra kết quả là một khoảng lớn hoặc nhỏ không đồng nhất vì nó phụ thuộc vào vị trí của số 0 tại MAJOR, MINOR hay PATCH trong version lúc được add vào package.json, ví dụ:

^2.5.2 nghĩa là >=2.5.2 && <3.0.0 (chấp nhận MAJOR thay đổi)
^1.0.5 nghĩa là >=1.0.5 && <2.0.0 (chấp nhận MINOR thay đổi)
^0.4.5 nghĩa là >=0.4.5 && <0.5.0 (chấp nhận PATCH thay đổi)

Tham khảo thêm tại [semver - The semantic versioner for npm]()https://docs.npmjs.com/misc/semver) và [Package.json dependencies done right](https://blog.nodejitsu.com/package-dependencies-done-right/).





- Nên dùng biến tham chiếu để giúp code ngắn gọn và tăng performance

Don't:

```
res.body.data.should.be.a('object');
res.body.data.id.should.equal(product.id.toString());
res.body.data.type.should.equal('products');

res.body.data.attributes.should.be.a('object');
res.body.data.attributes.code.should.equal(product.code);
res.body.data.attributes.name.should.equal(product.name);
res.body.data.attributes.price.should.equal(product.price);
res.body.data.attributes.type.should.equal(product.type);
```

Do:

```
let bodyData = res.body.data;
bodyData.should.be.a('object');
bodyData.id.should.equal(product.id.toString());
bodyData.type.should.equal('products');

let bodyDataAttrs = bodyData.attributes;
bodyDataAttrs.should.be.a('object');
bodyDataAttrs.code.should.equal(product.code);
bodyDataAttrs.name.should.equal(product.name);
bodyDataAttrs.price.should.equal(product.price);
bodyDataAttrs.type.should.equal(product.type);
```
